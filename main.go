package main

import (
	"flag"
	"fmt"
	"math/rand"
	"os"
	"sync"
	"time"

	"github.com/gocarina/gocsv"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/olegsobchuk/sh_reports/config"
	"github.com/olegsobchuk/sh_reports/models"
	"github.com/olegsobchuk/sh_reports/services/aws"
)

const (
	// condition for biggest query in DB (production)
	sqlLimit = 20_000
)

func init() {
	// DB params
	flag.StringVar(&config.Host, "host", "localhost", "DB host")
	flag.StringVar(&config.DBUser, "dbuser", "postgres", "DB user")
	flag.StringVar(&config.DBName, "dbname", "", "DB name")
	flag.StringVar(&config.DBPass, "dbpass", "password", "DB password")
	flag.Parse()
	config.DBConnect()
}

func main() {
	defer config.DB.Close()
	var wg sync.WaitGroup
	// generate export
	for i := 0; i < 1; i++ {
		wg.Add(1)
		go generateCSV(&wg)
	}

	// waait till all goroutines finished
	wg.Wait()
}

func generateCSV(wg *sync.WaitGroup) {
	// generate rundom filename
	rand.Seed(time.Now().UnixNano())
	fileName := fmt.Sprintf("bookings_%v.csv", rand.Float64())

	var count int
	bookings := []models.Booking{}
	config.DB.Table("reports_bookings").Where("venue_id IN (?)", models.BookingVenueIDs).Count(&count)
	pages := count / sqlLimit // count pages for pagination

	exportFile, err := os.Create(fileName) // create export file
	if err != nil {
		panic(err)
	}
	defer exportFile.Close()

	for i := 0; i <= pages; i++ {
		// run SQL with Debugger
		config.DB.Debug().
			Table(models.BookingTableName).
			Where("venue_id IN (?)", models.BookingVenueIDs).
			Offset(i * sqlLimit).
			Limit(sqlLimit).
			Find(&bookings)
		// push data as CSV to file
		err := gocsv.MarshalFile(&bookings, exportFile)
		if err != nil {
			panic(err)
		}
	}
	awsSession, _ := aws.Session()
	str, err := aws.Upload(awsSession, fileName)
	if err != nil {
		panic(err)
	}
	fmt.Println(str)
	wg.Done()
}

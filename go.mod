module github.com/olegsobchuk/sh_reports

go 1.13

require (
	github.com/aws/aws-sdk-go v1.25.3
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gocarina/gocsv v0.0.0-20190927101021-3ecffd272576
	github.com/jinzhu/gorm v1.9.11
)

package config

import (
	"fmt"
	"log"

	"github.com/jinzhu/gorm"
)

var (
	// DB pointer to DB connector
	DB *gorm.DB
	// Host DB host
	Host string
	// DBUser DB username
	DBUser string
	// DBName DB name
	DBName string
	// DBPass password to DB
	DBPass string
	// ConnStr full string for connecting DB
	connStr string
)

func DBConnect() {
	if DB == nil {
		connStr = fmt.Sprintf("host=%s user=%s dbname=%s password=%s", Host, DBUser, DBName, DBPass)

		// Open DB connection
		var err error
		DB, err = gorm.Open("postgres", connStr)
		// defer DB.Close()
		if err != nil {
			log.Fatal(err)
		}
		if DB == nil {
			log.Fatal("Can't connect to DB")
		}
	}
}

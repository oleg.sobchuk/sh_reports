package models

import "time"

const (
	// BookingTableName is a name of table in DB
	BookingTableName = "reports_bookings"
	// BookingSQL is SQL for fetching booking records
	BookingSQL = "venue_id IN (%s)"
)

var (
	// BookingVenueIDs is venue_ids which uses to filter records
	BookingVenueIDs = []int{
		10119, 10200, 10065, 10248, 10172, 10230, 10160, 10159, 10246, 10149, 10120, 10176, 10014, 10179, 10228, 10129, 10082, 10314, 10255, 10250, 10256, 10096, 10089, 10090, 10057, 10240, 10217, 10153, 10100, 10125, 10115, 10009, 10421, 10144, 10069, 10212, 10224, 10229, 10191, 10241, 10015, 10111, 10116, 10146, 10158, 10223, 10509, 10155, 10099, 10080, 10023, 10117, 10131, 10222, 10061, 10133, 10178, 10190, 10077, 10121, 10203, 10247, 10169, 10012, 10154, 10021, 10056, 10244, 10067, 10242, 10209, 10185, 10075, 10011, 10081, 10505, 10206, 10128, 10249, 10101, 10102, 10163, 10127, 10174, 10134, 10173, 10105, 10093, 10130, 10150, 10254, 10253, 10218, 10512, 10513, 10104, 10162, 10161, 10175, 10207, 10103, 10219, 10258, 10148, 10210, 10365, 10214, 10245, 10126, 10197, 10184, 10167, 10192, 10063, 10107, 10201, 10215, 10094, 10064, 10503, 10386, 10141, 10018, 10237, 10114, 10147, 10198, 10113, 10059, 10165, 10514, 10085, 10086, 10079, 10084, 10078, 10112, 10019, 10074, 10177, 10022, 10017, 10108, 10251, 10171, 10232, 10145, 10252, 10180, 10239, 10243, 10193, 10238, 10205, 10418, 10156, 10168, 10506, 10151, 10194, 10008, 10095, 10199, 10213, 10143, 10196, 10315, 10010, 10072, 10071, 10088, 10227, 10087, 10233, 10511, 10091, 10231, 10070, 10181, 10097, 10204, 10202, 10124, 10211, 10060, 10504, 10220, 10195, 10122, 10259, 10502, 10170, 10225, 10110, 10183, 10208, 10152, 10236, 10182, 10058, 10188, 10186, 10132, 10507, 10508, 10062, 10083, 10106, 10164, 10123, 10092, 10510, 10138, 10139, 10136, 10140, 10137, 10135, 10020, 10016, 10073, 10189, 10068, 10216, 10118, 10257, 10235, 10166,
	}
)

// Booking is a struct needed for export from booking DB
type Booking struct {
	BookingID             int64     `scv:"booking_id"`
	StartDatetime         time.Time `scv:"start_datetime"`
	OrderID               int64     `scv:"order_id"`
	VenueID               int64     `scv:"venue_id"`
	VenueTitle            string    `scv:"venue_title"`
	InvoiceIndex          int64     `scv:"invoice_index"`
	InvoiceDate           time.Time `scv:"invoice_date"`
	CustomerName          string    `scv:"customer_name"`
	FacilityCategory      string    `scv:"facility_category"`
	EndDatetime           time.Time `scv:"end_datetime"`
	NetPrice              float64   `scv:"net_price"`
	Vat                   float64   `scv:"vat"`
	GrossPrice            float64   `scv:"gross_price"`
	DateOfPayment         time.Time `scv:"date_of_payment"`
	InternalOrderID       int64     `scv:"internal_order_id"`
	OfflinePaymentEnabled bool      `scv:"offline_payment_enabled"`
	OrderStatus           int64     `scv:"order_status"`
}

SH report generator

before run, should be set up ENV variables

`AWS_S3_BUCKET`, `AWS_S3_ACCESS_KEY_ID` and `AWS_S3_SECRET_ACCESS_KEY`

also take a look at AWS file access setting `public-read` or `private`

to run app

`go run main.go -host=localhost -dbuser=postgres -dbpass=password -dbname=schoolhire_db_name`

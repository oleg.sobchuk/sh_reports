package aws

import (
	"bytes"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/globalsign/mgo/bson"
)

// Upload saves a file to aws bucket and returns the url to the file and an error if there's any
func Upload(s *session.Session, fileName string) (string, error) {
	file, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	fileInfo, err := file.Stat()
	if err != nil {
		return "", err
	}
	buffer := make([]byte, fileInfo.Size())
	_, err = file.Read(buffer)
	if err != nil {
		return "", err
	}

	// create a unique file name for the file
	tempFileName := "test/booking_report_" + bson.NewObjectId().Hex() + filepath.Ext(fileInfo.Name())

	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(os.Getenv("AWS_S3_BUCKET")),
		Key:                  aws.String(tempFileName),
		ACL:                  aws.String("public-read"), // could be private or public-read if you want it to be access by only authorized users
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(int64(fileInfo.Size())),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
		StorageClass:         aws.String("INTELLIGENT_TIERING"),
	})
	if err != nil {
		return "", err
	}

	return tempFileName, err
}

// Session creates connection to S3
func Session() (*session.Session, error) {
	// create an AWS session which can be
	// reused if we're uploading many files
	s, err := session.NewSession(&aws.Config{
		Region: aws.String("eu-west-2"),
		Credentials: credentials.NewStaticCredentials(
			os.Getenv("AWS_S3_ACCESS_KEY_ID"),     // ACCESS_KEY_ID you can use from ENV => os.Getenv("AWS_S3_ACCESS_KEY_ID")
			os.Getenv("AWS_S3_SECRET_ACCESS_KEY"), // SECRET_ACCESS_KEY
			""),                                   // token can be left blank for now
	})
	if err != nil {
		log.Println("Could not upload file. Error:", err)
	}
	return s, err
}
